﻿using CanellaApiPrueba.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CanellaApiPrueba.Data
{
    public class ConfiguracionData
    {
        private string _connectionString;

        public ConfiguracionData(IConfiguration configuracion)
        {
            _connectionString = configuracion.GetConnectionString("DefaultConnection");
        }

        public async Task<List<Automovil>> ListaAutomovil()
        {
            using (SqlConnection sql = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("ListaAutomovil", sql))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    var response = new List<Automovil>();
                    await sql.OpenAsync();
                    // obteniendo los datos del procedimiento almacenado
                    using (var render = await cmd.ExecuteReaderAsync())
                    {
                        while (await render.ReadAsync())
                        {
                            response.Add(MapAutomovil(render));
                        }
                    }
                    return response;
                }
            }
        }

        public async Task<Automovil> GetAutoId(int Id)
        {
            using (SqlConnection sql = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("GetAutoId", sql))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", Id));
                    Automovil response = null;
                    await sql.OpenAsync();

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            response = MapAutomovil(reader);
                        }
                    }

                    return response;
                }
            }
        }

        public async Task CrearAuto(Automovil Auto)
        {
            using (SqlConnection sql = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("CrearAuto", sql))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Matricula", Auto.Matricula));
                    cmd.Parameters.Add(new SqlParameter("@Marca", Auto.Marca));
                    cmd.Parameters.Add(new SqlParameter("@Color", Auto.Color));
                    cmd.Parameters.Add(new SqlParameter("@Precio", Auto.Precio));
                    await sql.OpenAsync();
                    await cmd.ExecuteNonQueryAsync();
                    return;
                }
            }
        }

        public async Task EditarAuto(int Id, Automovil automovil)
        {
            using (SqlConnection sql = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("EditarAutomovil", sql))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", Id));
                    cmd.Parameters.Add(new SqlParameter("@Matricula", automovil.Matricula));
                    cmd.Parameters.Add(new SqlParameter("@Marca", automovil.Marca));
                    cmd.Parameters.Add(new SqlParameter("@Color", automovil.Color));
                    cmd.Parameters.Add(new SqlParameter("@Precio", automovil.Precio));
                    await sql.OpenAsync();
                    await cmd.ExecuteNonQueryAsync();
                    return;
                }
            }
        }

        public async Task DesactivarAutomovil(int Id)
        {
            using (SqlConnection sql = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DesactivarAutomovil", sql))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Id", Id));
                    await sql.OpenAsync();
                    await cmd.ExecuteNonQueryAsync();
                    return;
                }
            }
        }

        private Automovil MapAutomovil(SqlDataReader reader)
        {
            return new Automovil()
            {
                Id = (int)reader["Id"],
                Matricula = (string)reader["Matricula"],
                Color = (string)reader["Color"],
                Marca = (string)reader["Marca"],
                Precio = (decimal)reader["Precio"]
            };
        }
    }
}
