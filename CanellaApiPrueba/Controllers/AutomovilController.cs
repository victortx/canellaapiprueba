﻿using CanellaApiPrueba.Data;
using CanellaApiPrueba.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CanellaApiPrueba.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AutomovilController : ControllerBase
    {
        private readonly ConfiguracionData configuracionData;

        public AutomovilController(ConfiguracionData configuracionData)
        {
            this.configuracionData = configuracionData;
        }

        [HttpGet]
        public async Task<List<Automovil>> GetAutoLista()
        {
            return await configuracionData.ListaAutomovil();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Automovil>> GetAutoId(int id)
        {
            var response = await configuracionData.GetAutoId(id);
            if (response == null) { return NotFound(); }
            return response;
        }

        [HttpPost]
        public async Task CrearAuto([FromBody] Automovil Automovil)
        {
            await configuracionData.CrearAuto(Automovil);
        }

        [HttpPut("{id}")]
        public async Task EditarAuto(int id, [FromBody] Automovil Automovil)
        {
            await configuracionData.EditarAuto(id, Automovil);
        }

        [HttpDelete("{id}")]
        public async Task DesactivarAutomovil(int id)
        {
            await configuracionData.DesactivarAutomovil(id);
        }
    }
}
